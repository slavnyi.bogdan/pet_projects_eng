import pandas as pd
from qvd import qvd_reader
from tqdm.auto import tqdm
from datetime import date
import os

# Requirements
# 1. Regions must be filtered
# 2. drugs_id must be filtered 
# 3. dates must be filtered

# !!! Remember 
# 1. Convert ALL IDs to the str datatype
# 2. Convert in qvd needed columns to the int datatypes
# 3. Make the primary key check
# 4. Make log file with checks in form of json

# !!! Critical checks
# 1. Before export to the FTP you must check that you DO NOT export sales for
#all drugs_id, all regions and all dates. Otherwise block an export


path_qvd_month = '//qlikdev01/Audit_RU/Data_M/'
path_qvd_week = '//qlikdev01/Audit_RU/Data_W/'

export_path = 'S:/FTP/monastyrev/OUT/'

# Path on the local computer
export_path = 'data/OUT/'


# Filters
filter_start_year = '2021'
filter_start_month = '01' # If month is January - enter '01' not '1'
filter_start_date = date(int(filter_start_year), int(filter_start_month), 1)

filter_country_id = ['-1']
filter_region_id = ['132', '199', '211', '196', '218']

log_dict = {}


def check_primary_key(df, column_to_check):
    result = (df[column_to_check].nunique() == df.shape[0])
    #True/False
    return result 


def extract_drugs_id_filter():
    df_drugs_id_pri_customer = pd.read_csv(export_path + 'Монастырев_SKU.csv', 
                    encoding='windows-1251', sep=';', dtype='str', 
                    usecols=['Код поставщика', 'ID'])

    df_drugs_id_pri_customer = df_drugs_id_pri_customer.dropna(subset=['ID']).drop_duplicates(subset=['ID'])
    
    # Write the primary key check to the log file
    log_dict['drugs_id_is_unique'] = check_primary_key(df_drugs_id_pri_customer, 'ID')
    # IDs are str data type
    return df_drugs_id_pri_customer


def country_sales_month_export(df_drugs_id_pri_customer):
    df_sales_month_country_agg = pd.DataFrame()
    filename_length = 20
    for filename in os.listdir(path_qvd_month):
        if 'country_' in filename.lower() and len(filename) >= filename_length:
            year = int(filename.split('_')[1].split('.')[0][:4])
            month = int(filename.split('_')[1].split('.')[0][-2:])
            file_date = date(year, month, 1)
            # Filter date
            if file_date >= filter_start_date:
                columns_to_filter = ['PERIOD_ID', 'AREA_ID', 'DRUGS_ID', 'V_RUR', 'Q']
                print(f'Reading the {filename} file...')
                df_temp_country_sales = qvd_reader.read(path_qvd_month + filename)
                # Filter columns
                df_temp_country_sales = df_temp_country_sales.loc[:, columns_to_filter]
                # Filter country
                df_temp_country_sales = df_temp_country_sales \
                                .loc[df_temp_country_sales['AREA_ID'].isin(filter_country_id), :]
                # Filter drugs_id
                drugs_to_filter = df_drugs_id_pri_customer['ID']
                df_temp_country_sales = df_temp_country_sales \
                                .loc[df_temp_country_sales['DRUGS_ID'].isin(drugs_to_filter), :]
                # All columns are still strings. Convert to float
                df_temp_country_sales['V_RUR'] = df_temp_country_sales['V_RUR'].astype('float')
                df_temp_country_sales['Q'] = df_temp_country_sales['Q'].astype('float')

                # First concat 
                df_sales_month_country_agg = pd.concat([df_sales_month_country_agg, df_temp_country_sales])
                # Then group
                df_sales_month_country_agg = df_sales_month_country_agg \
                                        .groupby(['PERIOD_ID', 'AREA_ID', 'DRUGS_ID'], as_index=False) \
                                        .agg({'V_RUR': 'sum', 'Q': 'sum'})

    df_sales_month_country_agg['year'] = df_sales_month_country_agg['PERIOD_ID'].apply(lambda x: x[:4])
    df_sales_month_country_agg['month'] = df_sales_month_country_agg['PERIOD_ID'].apply(lambda x: int(x[-2:]))
    df_sales_month_country_agg = df_sales_month_country_agg.drop('PERIOD_ID', axis=1)

    save_to_file(df_sales_month_country_agg, export_path, 'sales_month_country.csv')
    print(f'The file was saved to the {export_path} path!')
    return df_sales_month_country_agg


def region_sales_month_export(df_drugs_id_pri_customer):
    df_sales_month_region_agg = pd.DataFrame()
    for filename in os.listdir(path_qvd_month):
        if 'reg_' in filename.lower():
            year = int(filename.split('_')[1].split('.')[0][:4])
            month = int(filename.split('_')[1].split('.')[0][-2:])
            file_date = date(year, month, 1)
            # Filter date
            if file_date >= filter_start_date:
                columns_to_filter = ['PERIOD_ID', 'AREA_ID', 'DRUGS_ID', 'V_RUR', 'Q']
                print(f'Reading the {filename} file...')
                df_temp_region_sales = qvd_reader.read(path_qvd_month + filename)
                # Filter columns
                df_temp_region_sales = df_temp_region_sales.loc[:, columns_to_filter]
                # Filter country
                df_temp_region_sales = df_temp_region_sales \
                                .loc[df_temp_region_sales['AREA_ID'].isin(filter_region_id), :]
                # Filter drugs_id
                drugs_to_filter = df_drugs_id_pri_customer['ID']
                df_temp_region_sales = df_temp_region_sales \
                                .loc[df_temp_region_sales['DRUGS_ID'].isin(drugs_to_filter), :]
                # All columns are still strings. Convert to float
                df_temp_region_sales['V_RUR'] = df_temp_region_sales['V_RUR'].astype('float')
                df_temp_region_sales['Q'] = df_temp_region_sales['Q'].astype('float')

                # First concat 
                df_sales_month_region_agg = pd.concat([df_sales_month_region_agg, df_temp_region_sales])
                # Then group
                df_sales_month_region_agg = df_sales_month_region_agg \
                                        .groupby(['PERIOD_ID', 'AREA_ID', 'DRUGS_ID'], as_index=False) \
                                        .agg({'V_RUR': 'sum', 'Q': 'sum'})
    
    print('The groups are created!')
    df_sales_month_region_agg['year'] = df_sales_month_region_agg['PERIOD_ID'].apply(lambda x: x[:4])
    df_sales_month_region_agg['month'] = df_sales_month_region_agg['PERIOD_ID'].apply(lambda x: int(x[-2:]))
    df_sales_month_region_agg = df_sales_month_region_agg.drop('PERIOD_ID', axis=1)

    save_to_file(df_sales_month_region_agg, export_path, 'sales_month_regions.csv')
    print(f'The file was saved to the {export_path} path!')
    return df_sales_month_region_agg


def save_to_file(df, path, filename):
    df.to_csv(path + filename, index=False)
    print(f'The file {filename} was saved to the {path} path!')


def main():
    df_drugs_id_pri_customer = extract_drugs_id_filter()
    print(df_drugs_id_pri_customer)
    df_sales_month_country_agg = country_sales_month_export(df_drugs_id_pri_customer)
    df_sales_month_region_agg = region_sales_month_export(df_drugs_id_pri_customer)
    print(log_dict)

if __name__ == "__main__":
    main()












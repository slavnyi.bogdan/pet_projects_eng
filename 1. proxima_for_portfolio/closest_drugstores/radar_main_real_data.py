import pandas as pd
import numpy as np
from tqdm.auto import tqdm

import warnings
warnings.filterwarnings("ignore")

data_path = 'data/'
test_data_path = 'data/export_data/files_for_checking/'
export_final_data_path = 'data/export_data/final_data/'

log_dict = {}


def read_basic_transform_file():
    df = pd.read_csv(data_path + 'pharmacy_list.csv')
    log_dict['read_shape'] = df.shape[0]
    check_primary_key(df)
    # dropna
    df = df.dropna(subset=['latitude'])
    df = df.dropna(subset=['longitude'])
    log_dict['after_drop_lat_lon_shape'] = df.shape[0]
    # Set latitude and longitude as float
    # df['latitude'] = df['latitude'].str.replace(',', '.').astype('float')
    # df['longitude'] = df['longitude'].str.replace(',', '.').astype('float')
    return df


def check_primary_key(df):
    if df.shape[0] == df['pharmacy_id'].nunique():
        print('All is OK! The primary_key is unique!')
        log_dict['primary_key_is_unique'] = 'YES'
    else:
        print('ERROR! The primary_key is not unique!')
        log_dict['primary_key_is_unique'] = 'NO'


def haversine(x1_longitude, y1_latitude, x2_longitude, y2_latitude,
             radius=6371):

        x1_longitude = np.radians(x1_longitude)
        y1_latitude = np.radians(y1_latitude)
        x2_longitude = np.radians(x2_longitude)
        y2_latitude = np.radians(y2_latitude)

        d = 2 * radius * np.arcsin(((np.sin((y2_latitude - y1_latitude) / 2))**2 +\
                            np.cos(y1_latitude)*np.cos(y2_latitude)*\
                            np.sin((x2_longitude - x1_longitude) / 2)**2) ** 0.5)
        return d


def create_catalog_df(df):
    df_catalog = df[['pharmacy_id', 'user_inn', 'latitude', 'longitude']]
    # Create "center of mass"
    df_catalog['center_lat'] = df['latitude'].mean()
    df_catalog['center_lon'] = df['longitude'].mean()
    # Calculate km distance from center
    df_catalog['distance_from_center'] = haversine(df_catalog['longitude'], df_catalog['latitude'], 
                                                    df_catalog['center_lon'], df_catalog['center_lat'])
    # Sort rows by distance
    df_catalog_sorted = df_catalog.sort_values('distance_from_center', ascending=False).reset_index()

    return df_catalog_sorted


def build_triples(df_catalog):
    # The catalog is sorted
    df_catalog_cut = df_catalog.copy()
    used_ids_list = []
    triple_group_count = 1
    df_catalog_groups = pd.DataFrame()
    for row in tqdm(df_catalog.itertuples()):
        row_org_id_1 = row[1]
        if row_org_id_1 not in used_ids_list:
            df_catalog_triples_temp_all = pd.DataFrame()
            row_index_1 = row.Index
            row_corp_id_1 = row[2]
            new_center_lat_1 = row[3]
            new_center_lon_1 = row[4]
            used_corp_ids = []

            used_corp_ids.append(row_corp_id_1)
            df_catalog_triples_temp_1 = pd.DataFrame({'pharmacy_id': [row_org_id_1],
                                'user_inn': [row_corp_id_1], 'latitude': [new_center_lat_1], 'longitude': [new_center_lon_1]})
            df_catalog_triples_temp_1['triple_group'] = triple_group_count
            # Append the used id to the used ids list
            used_ids_list.append(row_org_id_1)
            df_catalog_triples_temp_all = df_catalog_triples_temp_all.append(df_catalog_triples_temp_1)
            # Drop the used row
            df_catalog_cut = df_catalog_cut.drop(index=row_index_1)
            df_catalog_cut['new_distance'] = haversine(new_center_lon_1, new_center_lat_1, 
                                df_catalog_cut['longitude'], df_catalog_cut['latitude'])
            # The new catalog sort by distance from the new center
            df_catalog_cut = df_catalog_cut.sort_values('new_distance')
            df_catalog_triples_temp_2 = pd.DataFrame()
            pairs_found = 0
            # Go into catalog cut df without new center pharmacy
            for row_2 in df_catalog_cut.itertuples():
                row_index_2 = row_2.Index
                row_org_id_2 = row_2[1]
                row_corp_id_2 = row_2[2]
                if pairs_found >= 2:
                    break
                if row_corp_id_2 not in used_corp_ids:
                    used_ids_list.append(row_org_id_2)
                    used_corp_ids.append(row_corp_id_2)
                    df_catalog_triples_temp_2 = pd.DataFrame(df_catalog_cut.loc[row_index_2, :]).T
                    df_catalog_triples_temp_2['triple_group'] = triple_group_count
                    df_catalog_cut = df_catalog_cut.drop(index=row_index_2)
                    df_catalog_triples_temp_all = df_catalog_triples_temp_all.append(df_catalog_triples_temp_2)
                    pairs_found += 1
                else:
                    continue

            triple_group_count += 1
            
            df_catalog_groups = df_catalog_groups.append(df_catalog_triples_temp_all)
            # if triple_group_count > 2:
            #     break
        else:
            continue
    return df_catalog_groups


def save_df_to_file(df, data_path, new_file_name):
    df.to_csv(data_path + new_file_name, index=False)
    print(f'The file "{new_file_name}" was saved to the folder!')


def merge_data_to_main_df(df_catalog_groups, df):
    df_merged = df.merge(df_catalog_groups[['pharmacy_id', 'triple_group', 'new_distance']], 
                            left_on='pharmacy_id', right_on='pharmacy_id')
    return df_merged


def create_lonely_ids(df_merged):
    # If brick_count < 3, then it must be rebricked
    srs_lonely_triple_groups = df_merged.groupby('triple_group', as_index=False) \
        .agg({'user_inn': 'nunique'}) \
        .rename(columns={'user_inn': 'inn_count'}) \
        .query('inn_count < 3')['triple_group']
    
    return srs_lonely_triple_groups


def leave_only_full_triples(df_merged, srs_lonely_triple_groups):
    df_filtered_merged = df_merged[~df_merged['triple_group'].isin(srs_lonely_triple_groups)]
    log_dict['shape_after_drop_lonely'] = df_filtered_merged.shape[0]
    return df_filtered_merged


def check_all_triples_save_file(df_filtered_merged):
    min_count_unique_user_inn_in_group = df_filtered_merged.groupby('triple_group', as_index=False) \
        .agg({'user_inn': 'nunique'}) \
        .rename(columns={'user_inn': 'unique_user_inn_in_group'}) \
                                ['unique_user_inn_in_group'].min()
    log_dict['total_triple_chunks'] = df_filtered_merged['triple_group'].nunique()
    if min_count_unique_user_inn_in_group == 3:
        print(f'All is OK. The minimum unique user_inn in group is: {min_count_unique_user_inn_in_group}')
        save_df_to_file(df_filtered_merged, export_final_data_path, 'pharmacy_list_chunked_in_triples.csv')
        log_dict['triples_are_full_and_different_corp_ids'] = 'YES'
    else:
        print(f'WARNING! The minimum unique user_inn in group is: {min_count_unique_user_inn_in_group}. \
        The file is not saved. Correct the script!')
        log_dict['triples_are_full_and_different_corp_ids'] = 'NO'
    
    if df_filtered_merged.shape[0] == df_filtered_merged['pharmacy_id'].nunique():
        log_dict['primary_key_is_unique_final'] = 'YES'
    else:
        log_dict['primary_key_is_unique_final'] = 'NO'


def save_log_file(log_dict):
    log_dict = str(log_dict)
    with open(export_final_data_path + 'log_file.txt', 'w') as f:
        f.write(log_dict)
        print('The log file was saved to the folder!')


def main():
    df = read_basic_transform_file()
    df_catalog = create_catalog_df(df)
    df_catalog_groups = build_triples(df_catalog[['pharmacy_id', 'user_inn', 'latitude', 'longitude']])
    df_merged = merge_data_to_main_df(df_catalog_groups, df)
    srs_lonely_triple_groups = create_lonely_ids(df_merged)
    df_filtered_merged = leave_only_full_triples(df_merged, srs_lonely_triple_groups)
    check_all_triples_save_file(df_filtered_merged)
    save_log_file(log_dict)
    
if __name__ == '__main__':
    main()





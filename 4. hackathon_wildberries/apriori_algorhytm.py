import pandas as pd
import numpy as np
from tqdm.auto import tqdm
from efficient_apriori import apriori
from fpdf import FPDF
import os

PATH = f'D:\Job\job_seek\hackatons\wildberries'

# Before expanding the list, get rid of unnecessary characters. Since there are "mean" combinations of quotes in the dataset
def leave_letters(string):
    alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,_'
    new_string = ''
    for letter in string:
        if letter in alphabet:
            new_string += letter
    return new_string


def read_and_transform_data():
    df = pd.read_csv('data/WB_hack_3.csv', header=None)
    new_column_names = ['utc_event_time', 'utc_event_date', 'user_id', 'city_name', 'ecom_event_action', 
                    'ecom_id', 'ecom_brand', 'ecom_variant', 'ecom_currency', 'ecom_price100', 'ecom_qty', 
                    'ecom_grand_total100', 'os_manufacturer', 'device_type', 'traffic_src_kind', 'app_version', 
                    'net_type', 'locale']
    df.columns = new_column_names
   # Convert to datetime
    df['utc_event_time'] = pd.to_datetime(df['utc_event_time'])
    df['utc_event_date'] = pd.to_datetime(df['utc_event_date'])
    columns_to_clean = ['ecom_event_action', 'ecom_id', 'ecom_brand', 'ecom_variant',
                   'ecom_currency', 'ecom_price100', 'ecom_qty', 'ecom_grand_total100']

    for column_name in tqdm(columns_to_clean):
        df[column_name] = df[column_name].apply(leave_letters)

    # Turn "string" into "list"
    for column_name in tqdm(columns_to_clean):
        df[column_name] = df[column_name].apply(lambda x: x.split(','))

    # Using the explode method, expand the list in columns (the list is expanded and each element is entered on a new line)
    # In other words, now we have 1 event per line, 1 product, 1 price, 1 quantity ...
    df_full = df.explode(columns_to_clean)
    # To keep the ability to group by orders, we will make a new column "check_id"
    # Which is the conjugation of the date-time and the user ID. 
    df_full['check_id'] = df_full['utc_event_time'].astype('str') + df_full['user_id'].astype('str')

    # We have several currencies in the dataset. Therefore, we convert everything into rubles at the rate
    df_currency_table = pd.DataFrame({'ecom_currency': ['RUB', 'BYN', 'KZT', 'AMD', 'KGS'],
                                    'currency_rate': [1, 29.79, 0.17, 0.15, 0.86]})

    # Add the exchange rate to the main table
    df_full = df_full.merge(df_currency_table, on='ecom_currency')
    df_full['ecom_price100'] = df_full['ecom_price100'].astype('float')
    df_full['ecom_grand_total100'] = df_full['ecom_grand_total100'].astype('float')
    df_full['ecom_qty'] = df_full['ecom_qty'].astype('int')

    df_full['ecom_price100'] = df_full['ecom_price100'] * df_full['currency_rate']
    df_full['ecom_grand_total100'] = df_full['ecom_grand_total100'] * df_full['currency_rate']

    df_full['ecom_currency'] = 'RUB'

    df_full = df_full.drop('currency_rate', axis=1)
    df_purchase = df_full.query('ecom_event_action == "purchase"')

    df_full.to_csv('data/df_full_clean.csv', index=False)
    df_purchase.to_csv('data/df_purchase.csv', index=False)
    
    return df_purchase


def return_apriori_products(rule):
    rule_text = str(rule)
    rule_list = rule_text.replace('} -> {', '/').replace('} (', '/').replace('{', '').split('/')
    rule_list = rule_list[:2]
    return rule_list


def create_apriori_rules_pdf(df_purchase):
    srs_brands = df_purchase.groupby('check_id', as_index=False) \
                    .agg({'ecom_brand': 'unique'})['ecom_brand']
    
    print('Creating apriori rules!')
    transactions = list(map(tuple, srs_brands))

    itemsets, rules = apriori(transactions, min_support=0.001, min_confidence=0.3)

    apriori_text_string = ''

    for rule in rules:
        first_product = return_apriori_products(rule)[0]
        second_product = return_apriori_products(rule)[1]
        confidence = round(rule.confidence * 100, 1)
        support = round(rule.support * 100, 2)
        lift = round(rule.lift, 1)
        
        apriori_text_string += f'When the product "{first_product}" is bought in {confidence}% cases the product "{second_product}" is bought. The probability to take the product "{second_product}" is {lift} times more than to take the "{first_product}" in comparisson to average sales. All these cases take {support}% from all checks\n\n'

    pdf = FPDF()
    pdf.compress = False
    pdf.add_page()
    pdf.set_font('Arial', '', size=12)
    pdf.write(5, apriori_text_string)
    pdf.output('reports/apriori_algorhytm.pdf')
    print('PDF File was created!')


def main():
    if 'df_purchase.csv' in os.listdir(PATH + '/data'):
        df_purchase = pd.read_csv('data/df_purchase.csv')
    else:
        df_purchase = read_and_transform_data()

    create_apriori_rules_pdf(df_purchase)


main()

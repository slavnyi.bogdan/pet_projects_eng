#!!! The "fbprophet" library is installed without dancing with a tambourine, only with the help of the ANACONDA environment
# To install the library in the ANACONDA Prompt terminal, run the command
# "conda install -c conda-forge fbprophet"
# You should also run the facebook_prophet.py script from the ANACONDA.Navigator environment,
# using terminal CMD.exe Prompt

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from tqdm.auto import tqdm
from fbprophet import Prophet
from fpdf import FPDF
import os
import holidays

PATH = f'D:\Job\job_seek\hackatons\wildberries'

# Before expanding the list, get rid of unnecessary characters. Since there are "mean" combinations of quotes in the dataset
def leave_letters(string):
    alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,_'
    new_string = ''
    for letter in string:
        if letter in alphabet:
            new_string += letter
    return new_string


def read_and_transform_data():
    df = pd.read_csv('data/WB_hack_3.csv', header=None)
    new_column_names = ['utc_event_time', 'utc_event_date', 'user_id', 'city_name', 'ecom_event_action', 
                    'ecom_id', 'ecom_brand', 'ecom_variant', 'ecom_currency', 'ecom_price100', 'ecom_qty', 
                    'ecom_grand_total100', 'os_manufacturer', 'device_type', 'traffic_src_kind', 'app_version', 
                    'net_type', 'locale']
    df.columns = new_column_names
    # Convert to datetime
    df['utc_event_time'] = pd.to_datetime(df['utc_event_time'])
    df['utc_event_date'] = pd.to_datetime(df['utc_event_date'])
    columns_to_clean = ['ecom_event_action', 'ecom_id', 'ecom_brand', 'ecom_variant',
                   'ecom_currency', 'ecom_price100', 'ecom_qty', 'ecom_grand_total100']

    for column_name in tqdm(columns_to_clean):
        df[column_name] = df[column_name].apply(leave_letters)

    # Turn "string" into "list"
    for column_name in tqdm(columns_to_clean):
        df[column_name] = df[column_name].apply(lambda x: x.split(','))

    # Using the explode method, expand the list in columns (the list is expanded and each element is entered on a new line)
    # In other words, now we have 1 event per line, 1 product, 1 price, 1 quantity ...
    df_full = df.explode(columns_to_clean)
    # To keep the ability to group by orders, we will make a new column "check_id"
    # Which is the conjugation of the date-time and the user ID.
    df_full['check_id'] = df_full['utc_event_time'].astype('str') + df_full['user_id'].astype('str')

    # We have several currencies in the dataset. Therefore, we convert everything into rubles at the rate
    df_currency_table = pd.DataFrame({'ecom_currency': ['RUB', 'BYN', 'KZT', 'AMD', 'KGS'],
                                    'currency_rate': [1, 29.79, 0.17, 0.15, 0.86]})

    # Add the exchange rate to the main table
    df_full = df_full.merge(df_currency_table, on='ecom_currency')
    df_full['ecom_price100'] = df_full['ecom_price100'].astype('float')
    df_full['ecom_grand_total100'] = df_full['ecom_grand_total100'].astype('float')
    df_full['ecom_qty'] = df_full['ecom_qty'].astype('int')

    df_full['ecom_price100'] = df_full['ecom_price100'] * df_full['currency_rate']
    df_full['ecom_grand_total100'] = df_full['ecom_grand_total100'] * df_full['currency_rate']

    df_full['ecom_currency'] = 'RUB'

    df_full = df_full.drop('currency_rate', axis=1)
    df_purchase = df_full.query('ecom_event_action == "purchase"')

    df_full.to_csv('data/df_full_clean.csv', index=False)
    df_purchase.to_csv('data/df_purchase.csv', index=False)
    
    return df_purchase


def build_holidays():
    # We throw holidays, for their accounting with a model
    holidays_dict = holidays.RU(years=(2021, 2022))
    df_holidays = pd.DataFrame.from_dict(holidays_dict, orient='index') \
        .reset_index()
    df_holidays = df_holidays.rename({'index':'ds', 0:'holiday'}, axis ='columns')
    df_holidays['ds'] = pd.to_datetime(df_holidays.ds)
    df_holidays = df_holidays.sort_values(by=['ds'])
    df_holidays = df_holidays.reset_index(drop=True)
    return df_holidays


def build_daily_sales_table(df_purchase):
    grp_daily_sales_qty = df_purchase.groupby('utc_event_date', as_index=True) \
                                    .agg({'ecom_qty': 'sum'})
    return grp_daily_sales_qty


def build_sales_graph_qty(grp_daily_sales_qty):

    sns.lineplot(data=grp_daily_sales_qty, x='utc_event_date', y='ecom_qty')
    plt.xlabel('date')
    plt.xticks(rotation=90)
    plt.savefig('sales_graph_qty.png', bbox_inches='tight')


def build_influencers_graph_return_mean_error(grp_daily_sales_qty, df_holidays):
    grp_daily_sales_for_prophet = grp_daily_sales_qty.reset_index().rename(columns={'utc_event_date': 'ds', 
                                                                  'ecom_qty': 'y'})

    # The period we need to cut off and predict (model check)
    predictions = 14

    # Cut off the last N points from the training sample to measure the quality on them
    train_df = grp_daily_sales_for_prophet[:-predictions]
    # Set up prophet - tell him to take into account holidays and seasonality
    # Since the data is only 2 months old, it is unfortunately not possible to add yearly seasonality
    m = Prophet(holidays=df_holidays, daily_seasonality=False, weekly_seasonality=True, yearly_seasonality=False)
    m.fit(train_df)
    # Predict 30 days
    future = m.make_future_dataframe(periods=predictions)
    forecast = m.predict(future)
    # Draw
    m.plot_components(forecast)
    plt.savefig('sales_influencers.png', bbox_inches='tight')


def build_prediction_graph(grp_daily_sales_qty, df_holidays):
    # Forecast for the full period (specify the number of days)
    prediction_days = 30
    grp_daily_sales_for_prophet = grp_daily_sales_qty.reset_index().rename(columns={'utc_event_date': 'ds', 
                                                                  'ecom_qty': 'y'})
    final_train_df = grp_daily_sales_for_prophet
    f = Prophet(holidays=df_holidays, daily_seasonality=False, weekly_seasonality=True, yearly_seasonality=False)
    f.fit(final_train_df)
    final_future = f.make_future_dataframe(periods=prediction_days)
    final_forecast = f.predict(final_future)
    # We look at what was predicted for the full period
    f.plot(final_forecast)
    plt.title('Forecast')
    plt.xlabel('date')
    plt.ylabel('qty_purchased')
    plt.savefig('prediction_graph.png', bbox_inches='tight')


def build_pdf_report():
    WIDTH = 210
    HEIGHT = 297

    pdf = FPDF()
    pdf.add_page()
    pdf.image('sales_influencers.png', 0, 30, WIDTH-5)
    pdf.add_page()
    pdf.image('prediction_graph.png', 0, 30, WIDTH-5)

    pdf.output('reports/fbprophet.pdf')
    print('PDF file was created!')


def main():
    if 'df_purchase.csv' in os.listdir(PATH + '/data'):
        df_purchase = pd.read_csv('data/df_purchase.csv')
    else:
        df_purchase = read_and_transform_data()

    df_holidays = build_holidays()
    grp_daily_sales_qty = build_daily_sales_table(df_purchase)

    build_sales_graph_qty(grp_daily_sales_qty)
    build_influencers_graph_return_mean_error(grp_daily_sales_qty, df_holidays)
    build_prediction_graph(grp_daily_sales_qty, df_holidays)
    build_pdf_report()

main()


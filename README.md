# Portfolio projects

# Projects description
## 1. Working project on Proxima International
## 1.1. Closest drugstores
 <u>Libraries used:</u> pandas, numpy  
  <u>Additionally:</u> Extract distance in km from two points that have geographical coordinates
 
 **Main task:** Build triple groups of the closest drugstores. The drugstores should be in different corporations.  Each drugstore has its geographical coordinates.
 
## 1.2. ETL task for the customer
 <u>Libraries used:</u> pandas, numpy, qvd, datetime, os  
 <u>Additionally:</u> ETL
 
 **Main task:** Extract data from the qvd files, transform according to the customers requirements and load to the FTP server.  
Warning! qvd files are compressed and use quite a lot of RAM in pandas. So, ETL procceses should not use a lot of RAM and be memory efficient.
 
## 2. Karpov Courses project
## 2.1. A/B testing (non-normal distribution) | Bootstrap
<u>Libraries used:</u> pandas, numpy, scipy, seaborn, os  
<u>Additionally:</u> bootstrap, chi-squared test, Shapiro-Wilk test, confidence interval, non-normal distribution

 **Main task:** While testing one hypothesis, the target group was offered a new mechanism for paying for services on the site, and
the control group retained the basic mechanics. As a task, you need to analyze the results of the experiment and decide whether it is worth launching a new payment mechanism for all users.

## 2.2. SQL for analytics (ARPU, ARPAU, CR)
<u>Libraries used:</u> pandas, numpy, os, sqlalchemy  
<u>Additionally:</u> SQL window (analytic functions), data generation

**Main task:** Generate data. Build query that gives information about users (ARPU, ARPAU, CR)

## 3. YouTube trend analysis
<u>Libraries used:</u> pandas, json, SQLAlchemy, seaborn, glob, os  
<u>Additionally:</u> Remove duplicates from the data. Generate new ID primary keys. Make visualizations with PowerBI. SQL window functions

**Main task:** In this task the questions from the marketing staff should be answered.  
There is a "Trends" tab on YouTube. Mostly "viral" videos get there - those that are rapidly gaining popularity. The YouTube algorithm, based on a huge number of factors, decides which video will fall into trends and which will not. These factors can be - an increase in the number of views, likes, reposts, the activity of comments ... Moreover, YouTube allocates daily video quotas for each country.  
The task is to process the dataset (put files - json, csv into one table), bring the data to the desired form. Determine what factors are decisive when a video hits trends. Answer questions that allow to look at the data from different angles.


## 4. Hackaton Wildberries
<u>Libraries used:</u> pandas, numpy, efficient_apriori, fbprophet, fpdf, os, seaborn
<u>Additionally:</u> Timeseries predictions. Related products search algorithm

**Main task:**  "Tool of the seller". Requirements:   
Design and implement a prototype of an analytical tool for sellers that will help them promote their products on the marketplace. The tool should improve the behavioral factors on the product page and increase sales. Create an analytical tool that can become an addition to the seller's account on Wildberries. We do not set product restrictions: the tool can be integrated into a personal account, or it can become an independent solution (a separate service, mobile application, voice assistant, etc.). The tool should help for sales increase, turnover acceleration, supply planning, control of financial indicators (profit, marginality, profitability of balances), improvement of behavioral factors on the product page

## 5. Customer churn analysis. Telecom
<u>Libraries used:</u> pandas, numpy, seaborn, sklearn  
<u>Additionally:</u> Correlation analysis. ML. Decision tree

**Main task:** This dataset represents a telecom company that wants to find the reasons and to reduce the customer churn.  
So, in the first part I will find the features that correlate with the customer churn (correlation does not imply causation). I will aslo build visualizations that will demonstrate the behaviour difference of each group. At the end of the part 1 there will be conclussions with suggested recomendations how to solve each problem.  
And in the second part I will build basic Machine Learning Model (Decission Tree) that "predicts" the customer churn according to customers behaviour with 94% accuracy.

## 6. Online hackaton by P&G. Check analysis. Apriori algorithm
<u>Libraries used:</u> pandas, numpy, efficient_apriori  
<u>Additionally:</u>  Association Rules Learning (ARL)  
**Main task:**   
**Association Rules Learning (ARL)** takes place on
a rule base that helps discover relationships between customer transactions across purchase history data. ARL analyzes the checks of individual consumers and identifies relationship rules.   
For example, if 80% of customers who have pizza on their receipt take and toothbrushes, then the following rule is obtained: "Buying a pizza is a condition for buying toothbrushes. At the same time, 80% is our confidence in the rule. But it happens it is useful to take into account not only confidence, but also support. Support is share buyers who took pizza and toothbrushes together, out of the total number of customers.

## 7. Holodom. Data transformation. Web scraping
<u>Libraries used:</u> pandas, BeautifulSoup, requests, lxml, os  
<u>Additionally:</u> ETL  
**Main task:** I made an ETL script that unites data from the two accounting systems. Additionaly I wrote a webscraper that collects the data from the currency exchange website. 













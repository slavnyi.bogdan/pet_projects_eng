import pandas as pd

PATH = f'D:/Job/git/pet_projects/holodom/'

def test_primary_keys():
    count_errors = 0
    df_customers_clean = pd.read_csv(PATH + 'data/ms_customers_clean.csv')
    df_products_clean = pd.read_csv(PATH + 'data/ms_products_clean.csv')
    df_exchange_rates = pd.read_csv(PATH + 'data/exchange_rates.csv')

    if df_customers_clean['Код'].nunique() != df_customers_clean.shape[0]:
        print('Ошибка уникальности ключа в таблице "Клиенты"')
        count_errors += 1
    if df_products_clean['Код'].nunique() != df_products_clean.shape[0]:
        print('Ошибка уникальности ключа в таблице "Товары"')
        count_errors += 1
    if df_exchange_rates['date'].nunique() != df_exchange_rates.shape[0]:
        print('Ошибка уникальности ключа в таблице "Курс валют"')
        count_errors

    print(f'Кол-во ошибок: {count_errors}')


def run_test():
    test_primary_keys()







